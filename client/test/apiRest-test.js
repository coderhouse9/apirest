const chai = require('chai');
const chaiHttp = require('chai-http');
const expect = require('chai').expect;
const request = require('supertest');

chai.use(chaiHttp);
const url = 'http://localhost:8080';

describe("Mostrar todos los productos: ", () => {
    it("Deberia mostrar todos los productos", (done) => {
      chai
        .request(url)
        .get("/api/productos")
        .end(function (err, res) {
          expect(res).to.have.status(200);
          done();
        });
    });
  });
  
  describe("Agregar producto: ", () => {
    it("Deberia agregar un producto", (done) => {
      chai
        .request(url)
        .post("/api/productos")
          .send({
            title: "Microscopio Optico",
            thumbnail:
              "https://imgs.search.brave.com/Ck67_PdJ3subdcJuS96IKo9u55-PVi3HM7O8-oWDY-o/rs:fit:1200:1200:1/g:ce/aHR0cHM6Ly9jZG4u/c2hvcGlmeS5jb20v/cy9maWxlcy8xLzE2/MTgvODU5Ny9wcm9k/dWN0cy9WRS1UMV8x/XzEwMjR4MTAyNEAy/eC5wbmc_dj0xNTY0/NDEwMDI0",
            price: 15000,
          })
        .end( (err, res) =>{
          expect(res).to.have.status(201);
          done();
        });
    });
  });
  
  describe("Actualizar producto por id: ", () => {
    it("Deberia actualizar un producto por id", (done) => {
        request(url)
        .put("/api/productos/632a05b2d79a45c00def0df3")
        .send({
            title: "Microscopio",
            price: 4567,
            thumbnail:
              "https://www.zeigenmx.com/images/thumbs/0003762_microscopio-binocular-biologico-plan-acromatico-al-infinito.jpeg",
          })
        .expect(200, done)
        });
  });
  
  describe("Eliminar producto por id: ", () => {
    it("Deberia eliminar un producto por id", (done) => {
      request(url)
        .delete("/api/productos/632a05b2d79a45c00def0df3")
        .expect(200, done);
    });
  });